class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def mark=(mark)
    @mark = mark
  end

  def display(board)
    @board = board
  end

  def get_move
    winning_sequences =  [
      [[0,0],[0,1],[0,2]],
      [[0,0],[1,1],[2,2]],
      [[1,0],[1,1],[1,2]],
      [[2,0],[2,1],[2,2]],
      [[0,2],[1,1],[2,0]],
      [[0,2],[1,2],[2,2]],
      [[0,0],[1,0],[2,0]],
      [[0,1],[1,1],[2,1]]
      ]
    computer_possible_winning = []
    winning_sequences.each do | winning_sequence|
      computer_possible_winning = []
      winning_sequence.each do |item|
        if @board.grid[item[0]][item[1]] == @mark
          computer_possible_winning.push(item)
          if computer_possible_winning.length == 2
            result =  winning_sequence -  computer_possible_winning
            return result.flatten
          end
        end
      end
    end
    i = 0
    j = 1
    while i < 3
      while j < 3
        if @board.grid[i][j] == nil
          return [i,j]
        end
        j+=1
      end
      i+=1
    end
  end

  def wins?(move)
    board[move] = @mark

    if board.winner == @mark
      board[move] = nil
      true
    else
      board[move] = nil
      false
    end
  end
end
