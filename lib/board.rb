class Board
  attr_accessor :grid, :marks

  def initialize(grid = nil)
    if grid.nil?
      @grid = Array.new(3) { Array.new(3) }
    else
      @grid = grid
    end
    @marks = [:X, :O]
  end

  def [](row, col)
    grid[row][col]
  end

  def []=(row, col, mark)
    grid[row][col] = mark
  end

  def place_mark(position, mark)
    self[*position] = mark if self.empty?(position)
  end

  def empty?(position)
    self[*position] == nil
  end

  def winner
    if win_a_row?(:X) || win_a_column?(:X) || win_a_diagonal?(:X)
      return :X
    elsif win_a_row?(:O) || win_a_column?(:O) || win_a_diagonal?(:O)
      return :O
    else
      nil
    end
  end

  def win_a_row?(player)
    grid.any? { |row| row.all? {|mark| mark == player }}
  end

  def win_a_column?(player)
    grid.transpose.any? { |row| row.all? { |mark| mark == player }}
  end

  def win_a_diagonal?(player)
    (0...grid.length).all? { |diag| grid[diag][diag] == player } || (0...grid.length).all? { |row| grid[row][(grid.length - 1) - row] == player }
  end

  def over?
    if self.winner != nil
      return true
    end
    grid.flatten.none? { |mark| mark.nil? }
  end
end
