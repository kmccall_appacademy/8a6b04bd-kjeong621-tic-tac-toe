class HumanPlayer
  attr_accessor :name, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    board.grid.each do |row|
      row.each do |mark|
        mark.nil? ? print("-") : print(mark)
      end
      puts ""
    end
    puts ""
  end

  def get_move
    puts "Where do you want to put your mark?"
    gets.chomp.split(', ').map(&:to_i)
  end
end
