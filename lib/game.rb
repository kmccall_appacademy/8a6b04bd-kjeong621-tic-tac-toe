require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player1, :player2, :current_player

  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    player1.mark = :X
    player2.mark = :O
    @current_player = player1
    @board = Board.new
  end

  def play
    current_player.display(board)

    play_turn until board.over?

    if board.winner
      board.winner.display(board)
      if board.winner == player1.mark
        puts "#{player1.name} wins!"
      else
        puts "#{player2.name} wins!"
      end
    else
      puts "Tie game. Play again."
    end
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def switch_players!
    @current_player = (current_player == player1 ? player2 : player1)
  end
end

if $PROGRAM_NAME == __FILE__
  print "Please enter your name: "
  name = gets.chomp.strip
  human = HumanPlayer.new(name)
  comp = ComputerPlayer.new('comp')

  game = Game.new(human, comp)
  game.play
end
